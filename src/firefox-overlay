#!/bin/sh

set -e

[ -n "${HOME}" ]
[ -n "${XDG_RUNTIME_DIR}" ]

OPT="$1"
TMP="${XDG_RUNTIME_DIR}/firefox-overlay"
LOW="${HOME}/.mozilla/firefox"

RST='\033[0;00m'
GRN='\033[0;32m'
YLW='\033[0;33m'

overlay_helper() {
  if [ -x "/usr/bin/doas" ]; then
    doas -n "@HELPER@" "$@" || return 1
  else
    sudo -n "@HELPER@" "$@" || return 1
  fi
}

overlay_mount() {
  if [ -e "${TMP}" ]; then
    die "overlay already mounted"
  fi

  # ungraceful shutdown
  if [ -L "${LOW}" ] && [ -d "${LOW}-lower" ]; then
    rm -f "${LOW}"

  # default state
  elif [ -d "${LOW}" ] && [ ! -e "${LOW}-lower" ]; then
    mv -n "${LOW}" "${LOW}-lower"

  else
    die "firefox not found"
  fi

  ln -s firefox-lower "${LOW}"

  install -d -m 0700 \
    "${TMP}/overlay" \
    "${TMP}/upper" \
    "${TMP}/work"

  if ! overlay_helper mount "${TMP}" "${LOW}-lower"; then
    rmdir \
      "${TMP}/overlay" \
      "${TMP}/upper" \
      "${TMP}/work" \
      "${TMP}"

    die "failed to mount overlay"
  fi

  rm -f "${LOW}"

  ln -s "${TMP}/overlay" "${LOW}"
}

overlay_unmount() {
  if [ -L "${LOW}" ] && [ -d "${LOW}-lower" ]; then
    rm -f "${LOW}"

    mv -n "${LOW}-lower" "${LOW}"
  fi

  if [ -e "${TMP}" ]; then
    if ! overlay_helper unmount "${TMP}"; then
      die "failed to unmount overlay"
    fi

    rm -rf "${TMP}"
  fi
}

overlay_flush() {
  if [ ! -f "${TMP}/overlay/profiles.ini" ]; then
    die "overlay not mounted"
  fi

  if [ ! -L "${LOW}" ] || [ ! -d "${LOW}-lower" ]; then
    die "overlay not mounted"
  fi

  if ! rsync -a --delete-after --inplace --no-whole-file "${TMP}/overlay/" "${LOW}-lower/"; then
    die "failed to flush overlay"
  fi
}

overlay_usage() {
  if [ ! -e "${TMP}" ]; then
    die "overlay not mounted"
  fi

  printf %b "${YLW}firefox${RST} (ssd) - "
  du -sh "${LOW}-lower" | awk '{print $1}'
  printf %b "${GRN}overlay${RST} (mem) - "
  du -sh "${TMP}/upper" | awk '{print $1}'
}

usage() {
  printf "%b\n" "${YLW}USAGE${RST}:"
  printf "%s\n" "    firefox-overlay [SUBCOMMAND]"
  printf "\n"
  printf "%b\n" "${YLW}SUBCOMMANDS${RST}:"
  printf "%b\n" "    ${GRN}m${RST}, ${GRN}mount${RST}      Mount overlay"
  printf "%b\n" "    ${GRN}u${RST}, ${GRN}unmount${RST}    Unmount overlay"
  printf "%b\n" "    ${GRN}f${RST}, ${GRN}flush${RST}      Flush overlay to disk"
  printf "%b\n" "    ${GRN}c${RST}, ${GRN}check${RST}      Check overlay status"
}

die() {
  printf "firefox-overlay: %s\n" "$*" >&2
  exit 1
}

case $OPT in
  m | mount)
    overlay_mount
    ;;
  u | unmount)
    overlay_unmount
    ;;
  f | flush)
    overlay_flush
    ;;
  c | check)
    overlay_usage
    ;;
  *)
    usage
    ;;
esac
